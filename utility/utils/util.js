import { db, auth, firebase } from '../../firebase.config'

function promise(data, type = 'resolve') {
  return new Promise((resolve, reject) => {
    if (type === 'resolve') {
      resolve(data)
    }

    if (type === 'reject') {
      reject(data)
    }
  })
}

const getItemsFromSnapshot = (snapshot) => {
  const isValidSnapshot = snapshot && snapshot.docs && snapshot.docs.length

  if (isValidSnapshot) {
    const rows = snapshot.docs.map((doc) => {
      return { ...doc.data(), id: doc.id }
    })

    return rows
  }

  return []
}

const getDocFromSnapshot = (doc) => {
  if (doc.id) {
    return { ...doc.data(), id: doc.id }
  }

  return undefined
}

const getUser = ({ docId, where }) => {
  let query = db.collection('users')

  if (docId) {
    return db
      .collection('users')
      .doc(docId)
      .get()
      .then((snapshot) => promise(getDocFromSnapshot(snapshot)))
  }

  if (where && where.length) {
    for (let i = 0; i < where.length; i += 1) {
      query = query.where(where[i][0], where[i][1], where[i][2])
    }
  }

  return query.get().then((snapshot) => promise(getItemsFromSnapshot(snapshot)))
}

const retrieveUser = async (uid) => {
  return await getUser({
    where: [['uid', '==', uid]]
  })
}

const getFieldValue = () => {
  return firebase.firestore.FieldValue
}

const addUser = ({ data }) => {
  let ref = db.collection('users')

  const add = (d) => {
    return ref.add(d).then((docRef) => promise({ ...d, id: docRef.id }))
  }

  if (data) {
    return add(data)
  }

  return promise(undefined)
}

const createItemAndAddUser = async (res) => {
  const item = {
    creationDate: getFieldValue().serverTimestamp(),
    uid: res.user.uid,
    twitterUsername: res.additionalUserInfo.username,
    profileImage: res.user.photoURL
  }

  return await addUser({ data: item })
}

export const signInWithTwitterAction = async () => {
  let userData
  const provider = new firebase.auth.TwitterAuthProvider()
  const response = await auth.signInWithPopup(provider).catch((err) => {
    return err
  })
  if (response.additionalUserInfo) {
    const user = await retrieveUser(response.user.uid || '')

    if (!user.length) {
      userData = await createItemAndAddUser(response)
    } else {
      userData = user[0]
    }
  }

  return { response, userData }
}

export const getInfoTwitter = async () => {
  const userData = []

  await db
    .collection('handles')
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        userData.push(doc.data())
      })
    })
    .catch(function (error) {
      console.log('Error getting documents: ', error)
    })

  return userData
}

export const addHandle = async (handle) => {
  try {
    const response = await db.collection('handles').add(handle)
    return response
  } catch (error) {
    return error
  }
}
