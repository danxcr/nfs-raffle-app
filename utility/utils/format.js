export const sortData = (buyers) => {
  buyers.sort((a, b) => {
    if (a.cantidad < b.cantidad) return 1
    if (b.cantidad < a.cantidad) return -1

    return 0
  })

  return buyers
}
