import { loadStripe } from '@stripe/stripe-js'

export const stripePromise = loadStripe(
  'pk_live_51ILBHDAI4DgTpKstmYrylFIYO1Pl4warKBIs1QCGAGsDgxHQEConr5VWbTytl0hZ73nfwVaoLUVWfk50Y36g0muD00O7cua3ur'
)
