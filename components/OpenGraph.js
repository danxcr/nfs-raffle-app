import { NextSeo } from 'next-seo'
import React from 'react'

export const OpenGraph = () => {
  return (
    <NextSeo
      title='ONE NFT'
      description='how many nft tickets are you going to buy?'
      canonical='https://nftraffle.xyz/'
      openGraph={{
        url: 'https://nftraffle.xyz/',
        title: 'NFT',
        description: 'how many nft tickets are you going to buy?',
        images: [
          {
            url:
              'https://res.cloudinary.com/dqioatpsk/image/upload/v1616422107/og_zouqra.png',
            width: 800,
            height: 600,
            alt: 'Nft Image'
          }
        ]
      }}
      twitter={{
        handle: '@coolhumansxyz',
        site: '@coolhumansxyz',
        cardType: 'summary_large_image'
      }}
    />
  )
}
