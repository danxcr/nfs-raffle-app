import React from 'react'
import Lottie from 'react-lottie';
import lotties from './../assets/lotties/index'

export default function LoattieConfetti() {
    // Lopttie options
    const lottieOptions = {
      loop: true,
      autoplay: true,
      animationData: lotties.confetti2,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
      },
    };
  
    return (
      <Lottie
style={{position: 'absolute', top: "0", pointerEvents: "none"}}
        options={lottieOptions}
        isStopped={false}
        isPaused={false}
      />
    );
  }
  