import React, { useContext } from 'react'
import Emoji from 'react-emoji-render'
import { TicketContext } from '../utility/context/TicketContext'
import { sortData } from '../utility/utils/format'

const RecentBuyers = () => {
  const { buyers } = useContext(TicketContext)

  return (
    <section className='recent-buyers'>
      <p>
        {' '}
        <Emoji text=':eyes:' />
        Recent buyers
      </p>
      <div className='recent-buyers-scroll'>
        {buyers.map((b, index) => (
          <div className='handle-cards' key={index}>
            <img src={b.data.profileImage || './placeholder.png'}></img>
            <div>
              <p> @{b.data.twitterUsername}</p>
              <p>has entered the raffle</p>
            </div>
          </div>
        ))}
      </div>
    </section>
  )
}

export default RecentBuyers
