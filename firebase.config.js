import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/analytics'

const firebaseConfig = {
  apiKey: 'AIzaSyDvsI8dK3gBypejDSXNDuMa-6_trjnmjHc',
  authDomain: 'nft-faffle.firebaseapp.com',
  projectId: 'nft-faffle',
  storageBucket: 'nft-faffle.appspot.com',
  messagingSenderId: '860350008121',
  appId: '1:860350008121:web:f0ffa9f49b18120ad276e8',
  measurementId: 'G-PZCV4ZZP70'
}
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
  // firebase.analytics()
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

export { db, auth, firebase, storage }
