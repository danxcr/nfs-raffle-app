import '../styles/globals.scss'
import '../styles/home.scss'
import '../styles/auth.scss'
import '../styles/confirmation.scss'

import { useState } from 'react'
import { UserContext } from '../utility/context/UserContext'
import 'firebase/analytics'
import { TicketContext } from '../utility/context/TicketContext'
import { OpenGraph } from '../components/OpenGraph'
import { useRouter } from 'next/router'

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({})
  const [handleConfirmed, setHandleConfirmed] = useState('')
  const [buyers, setBuyers] = useState([])
  const [tickets, setTickets] = useState(0)

  return (
    <TicketContext.Provider value={{ buyers, setBuyers, tickets, setTickets }}>
      <UserContext.Provider
        value={{
          user,
          setUser,
          handleConfirmed,
          setHandleConfirmed
        }}
      >
        <OpenGraph />
        <Component {...pageProps} />
      </UserContext.Provider>
    </TicketContext.Provider>
  )
}

export default MyApp
