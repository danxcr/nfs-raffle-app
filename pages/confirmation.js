import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import LottieConfetti from './../components/LottieConfetti'

const retrieveCheckoutFetcher = (url) => fetch(url).then((res) => res.json())

function confirmation() {
  const preTweet = 'http://twitter.com/intent/tweet?text='
  const router = useRouter()
  const [tweet, setTweet] = useState('')
  const sessionId = router.query?.session_id

  useEffect(async () => {
    if (sessionId) {
      const response = await retrieveCheckoutFetcher(`/api/checkout?sessionId=${sessionId}`)

      const metadata = response.metadata

      const url = `${preTweet}${encodeURI(
        `Just got ${
          metadata.quantity === '1' ? '1 ticket' : `${metadata.quantity} tickets`
        } from @coolhumansxyz at nftraffle.xyz`
      )}`
      setTweet(url)
    }
  }, [router.query])

  return (
    <div className='layout-center layout-confirmation'>
      <Head>
        <title>NFT Raffle</title>
        <link rel='stylesheet' href='https://use.typekit.net/ydn3kdw.css' />
      </Head>
      <LottieConfetti />
      <h1>You have entered the raffle!</h1>
      <a className='btn-twitter-confirmation' href={tweet} target='_blank'>
        <p>Share it on Twitter</p>
      </a>
    </div>
  )
}

export default confirmation
