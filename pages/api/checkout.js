const stripe = require('stripe')(process.env.STRIPE_SECRET)

export default async (req, res) => {
  const { method } = req

  switch (method) {
    case 'GET':
      const { sessionId } = req.query
      const retrieveSession = await stripe.checkout.sessions.retrieve(sessionId)
      res.send(retrieveSession)
      break
    case 'POST':
      const createSession = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: [
          {
            price_data: {
              currency: 'usd',
              product_data: {
                name: req.body.description
              },
              unit_amount: req.body.price
            },
            quantity: req.body.quantity
          }
        ],
        metadata: {
          id: req.body.metadata.id,
          uid: req.body.metadata.uid,
          username: req.body.metadata.username,
          profileImage: req.body.metadata.profileImage,
          quantity: req.body.metadata.quantity
        },
        mode: 'payment',
        success_url: `${process.env.URL}/confirmation?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${process.env.URL}/`
      })

      res.json({ id: createSession.id })
      break
    default:
      res.setHeader('Allow', ['GET', 'POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
