import dayjs from 'dayjs'
import { db } from '../../firebase.config'

export default async (req, res) => {
  const { body, method } = req

  switch (method) {
    case 'POST':
      switch (body.type) {
        case 'checkout.session.completed':
          const metadata = body.data.object.metadata
          const now = dayjs()

          for (let i = 0; i < Number(metadata.quantity); i++) {
            await db.collection('tickets').add({
              id: metadata.id,
              uid: metadata.uid,
              twitterUsername: metadata.username,
              profileImage: metadata.profileImage,
              creationDate: now.format()
            })
          }

          res.status(200).json({ status: 'ok' })
          break
        default:
          // Unexpected event type
          console.log(`Unhandled event type ${body.type}.`)
          res.status(200).json({ status: 'unhandled event' })
      }
      break
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
