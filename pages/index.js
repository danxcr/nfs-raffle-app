import Head from 'next/head'
import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import Emoji from 'react-emoji-render'
import { OpenGraph } from '../components/OpenGraph'
import { db } from '../firebase.config'
import { TicketContext } from '../utility/context/TicketContext'
import { UserContext } from '../utility/context/UserContext'
import { signInWithTwitterAction } from '../utility/utils/util'

import RecentBuyers from './../components/RecentBuyers'

export default function Home() {
  const router = useRouter()
  const { setUser } = useContext(UserContext)

  const { buyers, setBuyers } = useContext(TicketContext)

  const [error, setError] = useState(null)
  const [available, setAvailable] = useState(null)
  const [recent, setRecent] = useState(null)

  useEffect(() => {
    let tickets = []
    const getTickets = () => {
      db.collection('tickets')
        .get()
        .then((docSnapshot) => {
          docSnapshot.docs.map((doc) => {
            const value = tickets.some((t) => t.data.uid === doc.data().uid)
            if (!value) {
              tickets.push({ data: doc.data() })
            }
          })
          setBuyers(tickets)
        })
    }
    getTickets()
  }, [setBuyers])

  const handleAuth = async () => {
    const { response, userData } = await signInWithTwitterAction()
    if (userData) {
      setUser(userData)
      setAvailable('Login succeed')
      router.replace('/landing')
    } else {
      setError('Try again! Something went wrong...')
    }
  }

  return (
    <div className='layout-auth'>
      <Head>
        <title>NFT Raffle</title>
        <link rel='stylesheet' href='https://use.typekit.net/ydn3kdw.css' />
      </Head>
      <h1>
        One NFT. <br />
        100 tickets. <br /> one winner.
      </h1>
      <video
        playsInline
        loop
        autoPlay={true}
        muted
        title='https://foundation.app/coolhumansxyz/permanencia-by-lepapel-2092'
        src='https://res.cloudinary.com/dqioatpsk/video/upload/v1616030334/permanencia_lepapel_ocyv4c.mp4'
        onClick={() => {
          window.open('https://foundation.app/coolhumansxyz/permanencia-by-lepapel-2092', '_blank')
        }}
        type='video/mp4'
      ></video>
      <section>
        {error && (
          <button style={{ background: '#DB6060', fontSize: '16px' }}>
            {error}
            <Emoji text=':cry:' />
          </button>
        )}
        <button className='btn-twitter' onClick={handleAuth}>
          <img src='/image-twitter.svg'></img>
          Start with Twitter
        </button>
        <RecentBuyers />
      </section>
    </div>
  )
}
