import React, { useState, useEffect, useContext } from 'react'
import Head from 'next/head'
import Emoji from 'react-emoji-render'
import { useRouter } from 'next/router'

import RecentBuyers from './../components/RecentBuyers'
import { UserContext } from '../utility/context/UserContext'
import { db } from '../firebase.config'
import { TicketContext } from '../utility/context/TicketContext'
import { stripePromise } from '../utility/stripe'

const checkoutFetcher = (url, data) =>
  fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  }).then((res) => res.json())

const landing = () => {
  const { user } = useContext(UserContext)

  const { buyers, setBuyers, setTickets } = useContext(TicketContext)
  const router = useRouter()

  const [total, setTotal] = useState(100)
  const [counter, setCounter] = useState(0)
  const [errorMessage, setErrorMessage] = useState('')
  const [buy, setBuy] = useState(true)
  const [disableAdd, setDisableAdd] = useState(false)
  const [dataTickets, setDataTickets] = useState(null)

  useEffect(() => {
    let tickets = []
    const getTickets = async () => {
      db.collection('tickets').onSnapshot((docSnapshot) => {
        docSnapshot.docs.map((doc) => {
          const value = tickets.some((t) => t.data.uid === doc.data().uid)
          if (!value) {
            tickets.push({ data: doc.data() })
          }
        })
      })
    }
    setBuyers(tickets)
    getTickets()
  }, [setBuyers])

  //validations
  useEffect(() => {
    if (!user.id) {
      router.push('/')
    }
    let tickets = []

    const getTickets = async () => {
      db.collection('tickets').onSnapshot((docSnapshot) => setTotal(total - docSnapshot.docs.length))
      db.collection('tickets').onSnapshot((docSnapshot) => {
        docSnapshot.docs.map((doc) => {
          const value = tickets.some((t) => t.data.uid === doc.data().uid) //11

          if (value) {
            tickets.forEach((t) => {
              if (t.data.uid === doc.data().uid) {
                //11
                t.count += 1 // count: 2
              }
            })
          } else {
            tickets.push({ count: 1, data: doc.data() })
          }
        })
        if (tickets.some((t) => t.data.uid === user.uid)) {
          const ticket = tickets.find((t) => t.data.uid === user.uid)
          setDataTickets(ticket)
        }
      })
    }
    getTickets()
  }, [])

  useEffect(() => {
    if (dataTickets) {
      if (dataTickets.count === 1) {
        if (total === 2 && counter >= 2) {
          setDisableAdd(true)
          setErrorMessage('You can only buy 2 tickets')
        } else if (total === 1 && counter >= 1) {
          setDisableAdd(true)
          setErrorMessage('You can only buy 1 ticket')
        } else if (total === 0) {
          setDisableAdd(true)
          setErrorMessage('No more tickets available')
        } else {
          if (counter === 2) {
            setDisableAdd(true)
            setErrorMessage('You can buy 2 more tickets')
          }
          if (counter === 1) {
            setDisableAdd(false)
            setErrorMessage('')
          }
          if (counter === 0) {
            setDisableAdd(false)
            setErrorMessage('')
          }
        }
      }
      if (dataTickets.count === 2) {
        if (total === 2 && counter >= 2) {
          setDisableAdd(true)
          setErrorMessage('You can only buy 2 tickets')
        } else if (total === 1 && counter >= 1) {
          setDisableAdd(true)
          setErrorMessage('You can only buy 1 ticket')
        } else if (total === 0) {
          setDisableAdd(true)
          setErrorMessage('No more tickets available')
        } else {
          if (counter === 1) {
            setDisableAdd(true)
            setErrorMessage('You can only buy 1 more ticket')
          }
          if (counter === 0) {
            setDisableAdd(false)
            setErrorMessage('')
          }
        }
      }
      if (dataTickets.count >= 3) {
        setDisableAdd(true)
        setErrorMessage('You can only buy three tickets')
      }
    } else {
      if (total === 2 && counter >= 2) {
        setDisableAdd(true)
        setErrorMessage('You can only buy 2 tickets')
      } else if (total === 1 && counter >= 1) {
        setDisableAdd(true)
        setErrorMessage('You can only buy 1 ticket')
      } else if (total === 0) {
        setDisableAdd(true)
        setErrorMessage('No more tickets available')
      } else {
        setDisableAdd(false)
        setErrorMessage('')
      }
    }
  }, [dataTickets, counter, total])

  useEffect(() => {
    if (counter > 0 && counter < 4 && total != 0) {
      setBuy(true)
    } else {
      setBuy(false)
    }
  }, [counter])

  const handleBuy = async () => {
    const _data = {
      description: `Ticket NFT`,
      price: 1500,
      quantity: counter,
      metadata: {
        id: user.id,
        uid: user.uid,
        username: user.twitterUsername,
        quantity: counter,
        profileImage: user.profileImage
      }
    }

    // Get Stripe.js instance
    const stripe = await stripePromise

    // Call your backend to create the Checkout Session
    const response = await checkoutFetcher('/api/checkout', _data)

    const session = await response

    // When the customer clicks on the button, redirect them to Checkout.
    const result = await stripe.redirectToCheckout({
      sessionId: session.id
    })

    if (result.error) {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
      alert(result.error.message)
    }
  }

  const handleAdd = () => {
    const value = counter < 3 ? counter + 1 : counter
    setCounter(value)
  }

  const handleSubstract = () => {
    const value = counter > 0 ? counter - 1 : counter
    setCounter(value)
  }

  return (
    <div className='layout-home'>
      <Head>
        <title>NFT Raffle</title>
        <link rel='stylesheet' href='https://use.typekit.net/ydn3kdw.css' />
      </Head>

      <section className='main-home'>
        <h1>
          One NFT. <br /> 100 tickets. <br /> one winner.
        </h1>

        <div>
          <p>Ticket price</p>
          <h1>$15.00</h1>
          <div className='tickets-grid'>
            <section className='counter'>
              <div className='counter-input'>
                <button onClick={handleSubstract}>-</button>
                <input type='text' value={counter} onChange={() => {}}></input>
                <button onClick={handleAdd} disabled={disableAdd}>
                  +
                </button>
              </div>
              <p>
                <span>{total}</span> Tickets Available
              </p>
              {errorMessage && <div className='error-message'>{errorMessage}</div>}
            </section>
            <button className='btn-green' onClick={handleBuy} disabled={!buy}>
              <Emoji text=':admission_tickets:' />
              Purchase a Ticket
            </button>
          </div>

          <hr />
          <p>
            Winner will be announced on March 26th at 12:00 PM PST via Coolhumans Studio Twitter account. The token will
            be then transfered to the winners account.
          </p>
          <RecentBuyers buyers={buyers} />
        </div>

        <section className='nft-preview'>
          <video
            playsInline
            loop
            autoPlay={true}
            muted
            title='https://foundation.app/coolhumansxyz/permanencia-by-lepapel-2092'
            src='https://res.cloudinary.com/dqioatpsk/video/upload/v1616030334/permanencia_lepapel_ocyv4c.mp4'
            onClick={() => {
              window.open('https://foundation.app/coolhumansxyz/permanencia-by-lepapel-2092', '_blank')
            }}
            type='video/mp4'
          ></video>
        </section>
      </section>
    </div>
  )
}

export default landing
